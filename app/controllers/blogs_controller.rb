class BlogsController < ApplicationController
  # esto se ejecuta antes el metoro setblog para las aciones en el array
  # antes del metodo va a ejecutar esto
  before_action :set_blog, only: [:show, :edit, :update, :destroy]

  # GET /blogs
  # GET /blogs.json
  def index
    @blogs = Blog.all
  end

  # GET /blogs/1
  # GET /blogs/1.json
  #muestra el contenido del blog
  def show
  end

  # GET /blogs/new
  # no crea un nuevo blog, pero crea una instancia del blog, solo da la habilidad
  # de tener una nueva forma
  def new
    @blog = Blog.new
  end

  # GET /blogs/1/edit
  def edit
  end

  # POST /blogs
  # POST /blogs.json
  # esta accion en verdad crea un nuevo blog, toma los parametros de la forma
  def create
    @blog = Blog.new(blog_params)

    respond_to do |format|
      #valida que el blog sea valido
      if @blog.save
        # redirecciona al usuario
        format.html { redirect_to @blog, notice: 'Your post is live now' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /blogs/1
  # PATCH/PUT /blogs/1.json
  def update
    respond_to do |format|
      if @blog.update(blog_params)
        format.html { redirect_to @blog, notice: 'Blog was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /blogs/1
  # DELETE /blogs/1.json
  def destroy
    @blog.destroy
    respond_to do |format|
      format.html { redirect_to blogs_url, notice: 'Blog was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_blog
      @blog = Blog.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def blog_params
      params.require(:blog).permit(:title, :body)
    end
end
